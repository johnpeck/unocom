#include <stdio.h>

// Device-specific port definitions.  Also provides special
// bit-manipulations functions like bit_is_clear and
// loop_until_bit_is_set.
#include <avr/io.h>

// Library functions and macros for AVR interrupts
#include <avr/interrupt.h>

// Provides macros and functions for saving and reading data out of
// flash.
#include <avr/pgmspace.h>

#include "sound.h"

// Provides functions for working with the real time clock
#include "rtc.h"

// Provides logger_msg and logger_msg_p for log messages tagged with a
// system and severity.
#include "logger.h"

// Provide support for buttons on the 2x2-key click board
#include "buttons.h"

#include "spi.h"

// |--------------+----------+---------|
// | Click socket | Port pin | SPI pin |
// |--------------+----------+---------|
// | 1 and 2      | PB5      | SCK     |
// | 1 and 2      | PB4      | MISO    |
// | 1 and 2      | PB3      | MOSI    |
// | 1 only       | PB2      | SS      |
// |--------------+----------+---------|

void spi_init() {
  // Set MOSI, SCK, and SS to be outputs
  DDRB |= _BV(DDB5); // SCK
  DDRB |= _BV(DDB3); // MOSI
  DDRB |= _BV(DDB2); // SS

  // Enable SPI
  SPCR |= _BV(SPE);

  // Set the data order to be LSB first
  SPCR |= _BV(DORD);

  // Set master mode
  SPCR |= _BV(MSTR);

  // The clock idles low by default (CPOL = 0)

  // The data is sampled on the clock's rising edge (CPHA = 0) by default

  // Set SPI clock rate with 8MHz clock.  The clock frequency can be
  // up to 3 MHz
  SPSR |= _BV(SPI2X);  // Set double speed
  SPCR |= _BV(SPR0); // Set fosc / 8 = 1MHz 
}
