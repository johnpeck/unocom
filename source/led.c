


#include <stdio.h>

/* avr/io.h
   
   Device-specific port definitions.  Also provides special
   bit-manipulations functions like bit_is_clear and
   loop_until_bit_is_set.
*/
#include <avr/io.h>



// ----------------------- Functions ----------------------------------

/* led_init()

   Sets the data direction of the LED pin to be an output.
*/
void led_init(void) {
  DDRB |= _BV(DDB5);
}



/* led_on()

   Turns the led on.
*/
void led_on(void) {
  PORTB |= _BV(PORTB5);
}

/* led_off()

   Turns the led off.
*/
void led_off(void) {
  PORTB &= ~(_BV(PORTB5));
}

/* led_toggle()

   Changes the led state.
*/
void led_toggle(void) {
  if ( bit_is_set(PORTB,PORTB5) )
    led_off();
  else
    led_on();
}
