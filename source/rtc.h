#ifndef RTC_H
#define RTC_H



void rtc_init(void);

// Busy loop for the specified number of milliseconds.
uint16_t ms_delay( uint16_t );

// Returns a pointer to the value of the millisecond counter.
uint16_t *millis(void);

// Returns the elapsed time in milliseconds.
//
// Note that this can only deal with one rollover.  This needs to be
// called before two millisecond counter rollovers happen.  For a
// 16-bit value, this is roughly two minutes.
//
// Arguments:
//   old_ms -- Previous value of the millisecond counter
uint32_t elapsed_ms( uint16_t );

#endif // End the include guard
