#include <stdio.h>

// Device-specific port definitions.  Also provides special
// bit-manipulations functions like bit_is_clear and
// loop_until_bit_is_set.
#include <avr/io.h>

// Library functions and macros for AVR interrupts
#include <avr/interrupt.h>

// Provides macros and functions for saving and reading data out of
// flash.
#include <avr/pgmspace.h>

#include "sound.h"

// Provides functions for working with the real time clock
#include "rtc.h"

// Provides logger_msg and logger_msg_p for log messages tagged with a
// system and severity.
#include "logger.h"

// Provide support for buttons on the 2x2-key click board
#include "buttons.h"



// |---------------+----------------+------------+------------+-------|
// | Mikrobus name | Arduino socket | ATmega pin | 2x2 button | PCINT |
// |---------------+----------------+------------+------------+-------|
// | AN            |              2 | PC1        |          1 |     9 |
// | RST           |              2 | PC2        |          2 |    10 |
// | CS            |              2 | PB1        |          3 |     1 |
// | PWM           |              2 | PD5        |          4 |    21 |
// |---------------+----------------+------------+------------+-------|
//
// Note that the 2x2-Key click board also uses PD3 by default, which I
// need to make sounds.  The click board uses this line to indicate
// that any button was pressed.  Disable it by removing (desoldering)
// J2A.  This is a 0-ohm resistor next to the INT pin.
//
// Port pins will go from low to high when a button is pressed.


void buttons_init(buttons_state_t * buttons_state_ptr) {

  // Make button 1 an input
  DDRC &= ~(_BV(DDC1));

  // Make button 2 an input
  DDRC &= ~(_BV(DDC2));

  // Make button 3 an input
  DDRB &= ~(_BV(DDB1));
  
  // Make button 4 an input
  DDRD &= ~(_BV(DDD5));

  PCMSK0 = 0;
  PCMSK1 = 0;
  PCMSK2 = 0;
  
  // Open pin change interrupt mask for button 1
  PCMSK1 |= _BV(PCINT9);

  // Open pin change interrupt mask for button 2
  PCMSK1 |= _BV(PCINT10);

  // Open pin change interrupt mask for button 3
  PCMSK0 |= _BV(PCINT1);

  // Open pin change interrupt mask for button 4
  PCMSK2 |= _BV(PCINT21);
  
  // Enable all pin change interrupts
  PCICR |= _BV(PCIE0) | _BV(PCIE1) | _BV(PCIE2);

  buttons_state_ptr -> button_pushes[0] = 0;
  buttons_state_ptr -> button_pushes[1] = 0;
  buttons_state_ptr -> button_pushes[2] = 0;
  buttons_state_ptr -> button_pushes[3] = 0;
  
  buttons_state_ptr -> button_timestamps[0] = 0;
  buttons_state_ptr -> button_timestamps[1] = 0;
  buttons_state_ptr -> button_timestamps[2] = 0;
  buttons_state_ptr -> button_timestamps[3] = 0;
}

void buttons_scan(buttons_state_t * buttons_state_ptr, sound_state_t * sound_state_ptr) {
  uint16_t timenow_ms = *millis();
  loop_until_bit_is_set(TIFR0, TOV0);
  if (PINC & _BV(PC1)) {
    // Button 1 is showing "pushed"
    uint16_t timestamp_ms = buttons_state_ptr -> button_timestamps[0];
    if (elapsed_ms(timestamp_ms) >= 1000) {
      // It's been 1s since the last button push, so this is a new push.
      logger_msg_p("buttons", log_level_INFO, PSTR("Button 1 was pressed.\r\n"));
      sound_play_button1(sound_state_ptr);
      buttons_state_ptr -> button_timestamps[0] = timenow_ms;
    }
    return;
  }
  if (PINC & _BV(PC2)) {
    // Button 2 is showing "pushed"
    uint16_t timestamp_ms = buttons_state_ptr -> button_timestamps[1];
    if (elapsed_ms(timestamp_ms) >= 1000) {
      // It's been 1s since the last button push, so this is a new push.
      logger_msg_p("buttons", log_level_INFO, PSTR("Button 2 was pressed.\r\n"));      
      buttons_state_ptr -> button_timestamps[1] = timenow_ms;
    }
    return;
  }
  if (PINB & _BV(PB1)) {
    // Button 3 is showing "pushed"
    uint16_t timestamp_ms = buttons_state_ptr -> button_timestamps[2];
    if (elapsed_ms(timestamp_ms) >= 1000) {
      // It's been 1s since the last button push, so this is a new push.
      logger_msg_p("buttons", log_level_INFO, PSTR("Button 3 was pressed.\r\n"));
      buttons_state_ptr -> button_timestamps[2] = timenow_ms;
    }
    return;
  }
  if (PIND & _BV(PD5)) {
    // Button 4 is showing "pushed"
    uint16_t timestamp_ms = buttons_state_ptr -> button_timestamps[3];
    if (elapsed_ms(timestamp_ms) >= 1000) {
      // It's been 1s since the last button push, so this is a new push.
      logger_msg_p("buttons", log_level_INFO, PSTR("Button 4 was pressed.\r\n"));
      buttons_state_ptr -> button_timestamps[3] = timenow_ms;
    }
    return;
  }
}





ISR(PCINT1_vect) {
  // Interrupt for buttons 1 and 2
  
}

ISR(PCINT0_vect) {
  // Interrupt for button 3
  
}

ISR(PCINT2_vect) {
  // Interrupt for button 4
  
}
