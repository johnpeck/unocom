
#ifndef SOUND_H
#define SOUND_H

// Provide the bool data type
#include <stdbool.h>

// Definition of the sound status structure
//
// A structure of this type must be instantiated in the file
// containing main() so that it can work with the scheduler.
typedef struct sound_struct {
  // This will point to the next tone to be played.  Tone arrays
  // consist of alternating frequencies and sustain times.
  uint16_t * tone_ptr;
  
  // Millisecond counter when the tone was started
  uint16_t timestamp_ms;

  // How long the current tone should be played
  uint16_t timeout_ms;
  
  // This will be set to true when the sound is started.  It will be
  // set to false when the terminator is found in the tone array.
  bool sound_playing;
} sound_state_t;


/* sound_init()

   Configures timer1 to be clocked by the system clock divided down to
   1MHz.  Sets Clear Timer on Compare match (CTC) mode to make square
   waves with OC1A.  Sets the OCR1A value, but leaves the data
   direction of OC1A set to input (DDB5 bit in the DDRB register set
   to 0).  When playing a tone, set a new OC1A value, then change the
   port data direction.
*/
void sound_init(sound_state_t * sound_state_ptr);

// Start playing a tone array
//
// Arguments:
//   sound_state_ptr -- Pointer to the sound state structure
//   tone_array_ptr -- Pointer to the tone array
void sound_start(sound_state_t * sound_state_ptr, const uint16_t * tone_array_ptr);

// Update sound system
//
// Arguments:
//   sound_state_ptr -- Pointer to the sound state structure
void sound_update(sound_state_t * sound_state_ptr);

// Stop sound
//
// Arguments:
//   sound_state_ptr -- Pointer to the sound state structure
void sound_stop(sound_state_t * sound_state_ptr);

// Set the sound frequency
//
// Arguments:
//   frequency -- Frequency in Hz
void sound_set_frequency(uint16_t frequency);


/* sound_counter_stop()

   Stops the counter used for the sound module (timer1).  Does not
   reset it.
*/
void sound_counter_stop(void);

/* sound_counter_start()

   Starts the counter used for the sound module (timer1).  Configures
   prescaler for a 1MHz clock.
*/
void sound_counter_start(void);

/* sound_counter_zero()
   
   Clears the counter used for the sound module (timer1).
 */
void sound_counter_zero(void);



/* sound_play_timed( frequency (Hz), duration (ms))

   Play a tone for the specified amount of time.  Timer1 is clocked at
   1MHz, so the maximum frequency is 1MHz/2 = 500kHz.  The minimum frequency is
   1MHz/(2 * 65536) = 7.6Hz.

   fout = 1MHz / (2 * (1 + OCR1A)) so
   OCR1A = 1MHz / (2 * fout) - 1
 */
void sound_play_timed( uint16_t frequency, uint16_t duration_ms );


/* sound_play_array_p( array of frequency data) 

   Accepts a pointer to an array of 16-bit numbers located in flash
   memory.  Each number is a frequency in Hz, and each frequency will
   be played for 100ms.  To play a tone for 200ms, give the tone two
   entries.
*/
void sound_play_array_p( const uint16_t *data_ptr );


// Play the startup sound defined in sound.c
void sound_play_startup(sound_state_t * sound_state_ptr);

// Play the warning sound defined in sound.c
void sound_play_warn(sound_state_t * sound_state_ptr);

// Play the error sound defined in sound.c
void sound_play_error(sound_state_t * sound_state_ptr);

// Play a sound when button 1 is pressed
void sound_play_button1(sound_state_t * sound_state_ptr);


#endif // End the include guard
