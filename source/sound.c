// There's a piezo noisemaker on the hcardu0085 Arduino shield.  It's
// connected to Arduino pin 3 (PORTD3, OC2B), so we need to toggle
// that pin on a compare match to make sounds.

#include <stdio.h>

/* avr/io.h

   Device-specific port definitions
*/
#include <avr/io.h>

/* rtc.h

   Provides functions for working with the real time clock.
*/
#include "rtc.h"

/* logger.h

   Provides logger_msg and logger_msg_p for log messages tagged with a
   system and severity.
*/
#include "logger.h"

/* pgmspace.h

   Contains macros and functions for saving and reading data out of
   flash.
*/
#include <avr/pgmspace.h>

#include "sound.h"

//************************** Tone arrays ***************************//

// Each tone array is made of alternating frequency and sustain pairs.
// Frequencies are in Hz, sustains are in milliseconds.  A zero
// terminates the array.
//
// See sound_counter_start implementation for min and max frequencies.

// const uint16_t boot_sound[] PROGMEM = {200, 250, 250, 250, 300, 250, 0};
#include "scripts/tonegen.h"

const uint16_t warn_sound[] PROGMEM = {200, 100, 30, 100, 0};

const uint16_t error_sound[] PROGMEM = {300, 100, 200, 100, 0};

// const uint16_t button1_sound[] PROGMEM = {300, 100, 200, 100, 0};

// ----------------------- Functions ----------------------------------

void sound_init( sound_state_t * sound_state_ptr) {
  logger_msg_p("sound",log_level_INFO,
	       PSTR("Initializing sound\r\n"));

  // Set the system clock prescaler
  sound_counter_start();

  // Toggle OC2B on compare match
  TCCR2A |= _BV(WGM21);
  TCCR2A |= _BV(COM2B0);

  // Reset compare match flag by writing a 1 to it
  TIFR2 = _BV(OCF2B);

  // Set initial value for OCR2B
  OCR2B = 0xff;
  OCR2A = 0xff;

  // Initialize sound state
  sound_state_ptr -> sound_playing = false;
}

void sound_counter_stop(void) {
  // Set all CS values to 0 to remove clock source -- stoping counter
  TCCR2B &= ~(_BV(FOC2B) | _BV(CS22) | _BV(CS21) | _BV(CS20));
  // TCCR2B = 0;
}

void sound_counter_start(void) {
  // |-----------+--------+--------|
  // | Prescaler | Fmin   | Fmax   |
  // |-----------+--------+--------|
  // |         8 | 3.9kHz | 500kHz |
  // |        32 | 977Hz  | 125kHz |
  // |       128 | 244Hz  | 31kHz  |
  // |       256 | 122Hz  | 16kHz  |
  // |      1024 | 30Hz   | 3.9kHz |
  // |-----------+--------+--------|
  TCCR2B = (_BV(CS22) | _BV(CS21)); // 256
  // TCCR2B = (_BV(CS22) | _BV(CS21) | _BV(CS20)); // 1024
}

/* sound_counter_zero()

   Clears the counter used for the sound module (timer1).  Also clears
   the output compare flag OCF1A.
 */
void sound_counter_zero(void) {
  TCNT2 = 0;

  /* The timer2 output compare flag is cleared by writing a 1 to
     it. */
  TIFR2 |= _BV(OCF2B);
}

void sound_start( sound_state_t * sound_state_ptr,
		  const uint16_t * tone_array_ptr) {
  // Dummy initialization
  uint16_t frequency = 1;
  uint16_t sustain = 1;
  frequency = pgm_read_word(&(*tone_array_ptr));
  tone_array_ptr++;
  sustain = pgm_read_word(&(*tone_array_ptr));
  sound_state_ptr -> tone_ptr = tone_array_ptr;
  sound_state_ptr -> timestamp_ms = *millis();
  loop_until_bit_is_set(TIFR0, TOV0);
  sound_state_ptr -> timeout_ms = sustain;
  sound_state_ptr -> sound_playing = true;
  sound_set_frequency(frequency);
  DDRD |= _BV(DDD3); // Make PD3 (OC2B) an output
}

void sound_update( sound_state_t * sound_state_ptr) {
  if (sound_state_ptr -> sound_playing) {
    uint16_t timestamp_ms = sound_state_ptr -> timestamp_ms;
    uint16_t timeout_ms = sound_state_ptr -> timeout_ms;
    uint16_t now_played_ms = elapsed_ms(timestamp_ms);
    if (now_played_ms >= timeout_ms) {
      // We've played long enough.  Advance to the next tone.
      sound_state_ptr -> tone_ptr++;
      uint16_t frequency = pgm_read_word(&(*sound_state_ptr -> tone_ptr));
      if (frequency == 0) {
	// We're at the end of the array.  Stop the sound.
	sound_stop(sound_state_ptr);
	return;
      }
      sound_state_ptr -> tone_ptr++;
      uint16_t sustain = pgm_read_word(&(*sound_state_ptr -> tone_ptr));
      sound_state_ptr -> timeout_ms = sustain;
      // Set the new timestamp
      sound_state_ptr -> timestamp_ms = *millis();
      loop_until_bit_is_set(TIFR0, TOV0);
      sound_set_frequency(frequency);
      return;
    }
  } else {
    // There's no sound playing
    return;
  }
}

void sound_set_frequency( uint16_t frequency) {
  // freq = fosc / (Npre x 2 x (Ncmp + 1))
  //
  // The factor of 2 comes from the two toggles necessary to make one
  // period.  The Ncpm + 1 comes from the comparitor number being
  // zero-based. Npre is the prescaler, and Ncmp is the compare value.
  // Solving for Ncmp gives:
  //
  // Ncmp = fosc / (2 x Npre * freq) - 1
  //
  // ...and I'll neglect the 1.
  const uint16_t minfreq = 122; // Hz -- minimum possible freq
  const uint16_t maxfreq = 16000; // Hz -- maximum possible freq
  uint8_t compval = 0; // Will be the output compare value
  if ( (frequency >= minfreq) & (frequency <= maxfreq) ){
    // compval = (uint8_t) (3906 / frequency); // 1024 prescaler
    compval = (uint8_t) (15625 / frequency); // 256 prescaler
  }
  else {
    compval = 0xff;
  }
  // logger_msg_p("sound",log_level_INFO,
  // 	       PSTR("Compare value is %u\r\n"),compval);
  OCR2B = compval;
  OCR2A = compval + 1;
}

void sound_stop(sound_state_t * sound_state_ptr ) {
  DDRD &= ~(_BV(DDD3)); // Disable PD3
  sound_state_ptr -> sound_playing = false;
}

/* sound_play_timed( frequency (Hz), duration (ms))

   Play a tone for the specified amount of time.
 */
void sound_play_timed( uint16_t frequency, uint16_t duration_ms ) {
  /* Use the old_, new_, and all_ms variables to take care of pretty
     good timing.
   */
  const uint16_t minfreq = 30; // Hz -- minimum possible freq
  const uint16_t maxfreq = 3900; // Hz -- maximum possible freq
  // sound_counter_stop();
  sound_counter_zero();

  /* The sound is generated by the output compare pin (OC2B)
     toggling.  It changes state when the sound counter (timer2)
     reaches the compare value (OCR2B).  Calculating the compare
     value here is expensive, but not a big deal. */
  uint8_t compval = 0; // Will be the output compare value
  if ( (frequency >= minfreq) & (frequency <= maxfreq) ){
    compval = (uint8_t) (7813 / (2 * frequency)) - 1;
  }
  else {
    compval = 0xff;
  }
  logger_msg_p("sound",log_level_INFO,
	       PSTR("Compare value is %u\r\n"),compval);
  OCR2B = compval;
  OCR2A = compval + 1;
  // sound_counter_start();
  DDRD |= _BV(DDD3); // Make PD3 (OC2B) an output
  // Busy loop for the specified duration
  uint16_t retval = ms_delay( duration_ms );
  // sound_counter_stop(); // Stop the sound
  DDRD &= ~(_BV(DDD3)); // Disable PD3
  logger_msg_p("sound",log_level_INFO,
	       PSTR("Played for %u ms\r\n"),retval);
}

/* sound_play_array_p( array of frequency data)

   Accepts a pointer to an array of 16-bit numbers located in flash
   memory.  Each number is a frequency in Hz, and each frequency will
   be played for 100ms.  To play a tone for 200ms, give the tone two
   entries.
*/
void sound_play_array_p( const uint16_t *data_ptr ) {
  uint16_t snd_data = 1; // Dummy initialization values
  snd_data = pgm_read_word(&(*data_ptr));
  while ( (snd_data != 0) ) {
    sound_play_timed((uint16_t)(snd_data),100);
    data_ptr++;
    snd_data = pgm_read_word(&(*data_ptr));
  }
}

void sound_play_startup( sound_state_t * sound_state_ptr ) {
  sound_start( sound_state_ptr, boot_sound );
}

void sound_play_warn( sound_state_t * sound_state_ptr ) {
  sound_start( sound_state_ptr,  warn_sound );
}

void sound_play_error( sound_state_t * sound_state_ptr ) {
  sound_start( sound_state_ptr, error_sound );
}

void sound_play_button1( sound_state_t * sound_state_ptr) {
  sound_start( sound_state_ptr, button1_sound );
}

ISR(TIMER2_COMPB_vect) {
  logger_msg_p("sound",log_level_INFO,
	       PSTR("T2 match"));
  return;
}
