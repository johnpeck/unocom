// Real Time Clock (RTC) module

#include <stdio.h>

/* avr/io.h

   Device-specific port definitions.  Also provides special
   bit-manipulations functions like bit_is_clear and
   loop_until_bit_is_set.
*/
#include <avr/io.h>

// Library functions and macros for AVR interrupts
#include <avr/interrupt.h>

// Defines fixed-width integer types like uint8_t
#include <stdint.h>

/* pgmspace.h

   Provides macros and functions for saving and reading data out of
   flash.
*/
#include <avr/pgmspace.h>

/* logger.h

   Provides logger_msg and logger_msg_p for log messages tagged with a
   system and severity.
*/
#include "logger.h"

#include "rtc.h"

// Counter incremented by timer0 overflows every 256us
volatile uint16_t us_counter = 0;

// Counter incremented by timer0 overflows every 1ms
volatile uint16_t ms_counter = 0;

void rtc_init(void) {
  // Configures timer0 to be clocked by the system clock divided by
  // 8.  This means the 8-bit register will overflow every 1us.
  logger_msg_p("rtc",log_level_INFO,
	       PSTR("Initializing rtc\r\n"));

  // Configure the clock source for timer0 to be the system clock
  // prescaled by 8.
  TCCR0B |= _BV(CS01);

  // Enable overflow interrupts
  TIMSK0 |= _BV(TOIE0);

} // End rtc_init

uint16_t ms_delay( uint16_t duration_ms ) {
  // Provides a delay by polling millis()
  uint16_t old_ms = 0;
  uint16_t new_ms = 0;
  uint32_t all_ms = 0;

  old_ms = *millis();
  // Wait until the next 1/4 millisecond register update to avoid
  // contention.  This adds at most a 256us delay.
  loop_until_bit_is_set(TIFR0, TOV0);

  while ( all_ms < duration_ms ) {
    all_ms = elapsed_ms(old_ms);
  }
  return all_ms;
} // End ms_delay

uint16_t *millis(void) {
  // Return a value in ms
  return &ms_counter;
}

uint32_t elapsed_ms( uint16_t old_ms) {
  uint16_t new_ms = 0;
  uint32_t all_ms = 0;
  new_ms = *millis();
  // Wait until the next 1/4 millisecond register update to avoid
  // contention.  This adds at most a 256us delay.
  loop_until_bit_is_set(TIFR0, TOV0);

  if (new_ms > old_ms) {
    all_ms += (new_ms - old_ms);
  }
  if (new_ms < old_ms) {
    // We've rolled over, or had a timekeeping problem
    if ( (old_ms - new_ms) < 500 ) {
      // This is the bad situation.  We haven't rolled over, but
      // millis() returned a value from the recent past.
      logger_msg_p("rtc", log_level_WARNING, PSTR("Got time %u ms in the past.\r\n"),
		   old_ms - new_ms);
    } else {
      // We've rolled over
      all_ms += (0xffff - old_ms + new_ms); // We've rolled over
    }
    old_ms = new_ms;
  }
  return all_ms;
}

ISR(TIMER0_OVF_vect) {
  us_counter++;
  if (us_counter >= 4) {
    ms_counter++;
    us_counter = 0;
  }
  return;
}

