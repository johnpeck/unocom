#ifndef BUTTONS_H
#define BUTTONS_H

// Definition of the buttons status structure
//
// Keep track of how many times each button has been pushed.
typedef struct buttons_struct {
  // Each array location will contain the button push counts.
  uint16_t button_pushes[4];

  // Timestamps for the last button pushes
  uint16_t button_timestamps[4];
} buttons_state_t;


void buttons_init(buttons_state_t * buttons_state_ptr);

void buttons_scan(buttons_state_t * buttons_state_ptr, sound_state_t * sound_state_ptr);

#endif
