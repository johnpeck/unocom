
#include <stdio.h>
#include <string.h>
#include <avr/interrupt.h>

#include "functions.h"
#include "main.h"

/* clock.h

   Provides fosc_1mhz() and fosc_8mhz() for setting the system clock.
*/
#include "clock.h"

/* command.h

   Contains the extern declaration of command_array -- an array
   containing all the commands understood by the system.

   Defines the received command state structure recv_cmd_state_t.  Use
   this to keep track of the remote interface state. */
#include "command.h"
#include "usart.h"

/* pgmspace.h

   Contains macros and functions for saving and reading data out of
   flash.
*/
#include <avr/pgmspace.h>

/* logger.h

   Sets up logging
*/
#include "logger.h"

/* adc.h

   Provides functions for configuring and using the Butterfly's ADC
   module.
*/
// #include "adc.h"

/* rtc.h

   Provides functions for working with the real time clock.
*/
#include "rtc.h"

/* sound.h

   Provides functions for using the noisemaker.
*/
#include "sound.h"

/* eeprom.h

   Provides functions for writing to and reading from the eeprom.
*/
// #include "eeprom.h"

/* led.h

   Provides functions for turning the LED on and off.
*/
#include "led.h"

/* current.h

   Provides functions for measuring output current.
*/
#include "current.h"

// Provide support for buttons on the 2x2-key click board
#include "buttons.h"

/* Set the measurement array size.

   As things are now, this will also be the number of readings to
   average to create an output datum.  But this size can grow
   independent of the number of averages.
*/
#define MEASUREMENT_ARRAY_SIZE 4

/* This array will hold measurements made in the main loop.
 */
uint16_t measurement_array[MEASUREMENT_ARRAY_SIZE];

// Define a pointer to the received command state
recv_cmd_state_t  recv_cmd_state;
recv_cmd_state_t *recv_cmd_state_ptr = &recv_cmd_state;

// Pointer to the sound state
sound_state_t sound_state;
sound_state_t * sound_state_ptr = &sound_state;

// Pointer to the buttons state
buttons_state_t buttons_state;
buttons_state_t * buttons_state_ptr = &buttons_state;



int main() {
  int retval = 0;
  sei(); // Enable interrupts
  /* Set up the system clock.  Do this before setting up the USART,
     as the USART depends on this for an accurate buad rate.
  */
  fosc_8mhz();
  /* Set up the USART before setting up the logger -- the logger uses
     the USART for output.
  */
  usart_init(); // Sets 9.6k baud
  // usart_76k8_baud(); // Sets 76.8k baud
  
  // Initialize the logger.  Note that nothing can be logged before
  // this line.  Also note that the logger won't be able to make error
  // sounds until sound_init() is called.
  logger_init( sound_state_ptr );

  // Start the sound module, which uses timer2.
  sound_init( sound_state_ptr ); 

  // Start the rtc module, which uses timer0.
  rtc_init();

  // Start the SPI module
  spi_init();

  
  /* To configure the logger, first clear the logger enable register
     by disabling it with logger_disable().  Then set individual bits
     with logger_setsystem().
  */
  logger_disable(); // Disable logging from all systems
  logger_setsystem( "logger" ); // Enable logger system logging
  logger_setsystem( "rxchar" ); // Enable received character logging
  logger_setsystem( "command" ); // Enable command system logging
  logger_setsystem( "adc" ); // Enable adc module logging
  logger_setsystem( "sound" ); // Enable sound module logging
  logger_setsystem( "rtc" ); // Enable real time clock module logging
  logger_setsystem( "eeprom" ); // Enable eeprom module logging
  logger_setsystem( "cal" ); // Enable calibration module logging
  logger_setsystem( "buttons" ); // Enable buttons system logging
  functions_init(); // Load the serial number
  // adc_init(); // Set the ADCs reference and SAR prescaler
  // adc_mux(4); // Set the ADC mux to channel 4
  
  
  command_init( recv_cmd_state_ptr );

  // current_init();
  buttons_init( buttons_state_ptr );
  memset(measurement_array,0,MEASUREMENT_ARRAY_SIZE);

  // Play a startup sound after all the init functions have been called.
  sound_play_startup(sound_state_ptr);

  /* The main loop

     Use the old_, new_, and all_ms variables to take care of pretty
     good timing.
  */
  uint16_t old_ms = 0;
  uint16_t new_ms = 0;
  uint32_t all_ms = 0; // The elapsed time in milliseconds
  
  
  logger_msg_p("command", log_level_INFO, PSTR("Firmware version is %s\r\n"),
	       REVCODE);

  old_ms = *millis();
  // Wait until the next 1/4 millisecond register update to avoid
  // contention.  This adds at most a 256us delay.
  loop_until_bit_is_set(TIFR0, TOV0);
  
  
  
  for(;;) {
    /* Process the parse buffer to look for commands loaded with the
       received character ISR.
    */
    process_pbuffer( recv_cmd_state_ptr, command_array );
    sound_update(sound_state_ptr);

    buttons_scan(buttons_state_ptr, sound_state_ptr);
    
    all_ms = elapsed_ms(old_ms);
    if (all_ms >= 1000) {
      // Do something scheduled here.  The longest timing conditional
      // (all_ms < some_big_number) needs to reset the all_ms value.
      // led_toggle();
      // end of timed tasks
      old_ms = *millis();
      loop_until_bit_is_set(TIFR0, TOV0);
      all_ms = 0;
    }

  }// end main for loop
  return retval;
} // end main

/* -------------------------- Interrupts -------------------------------
 * Find the name of interrupt signals in iom328p.h.
 *
 * See the naming convention outlined at
 * http://www.nongnu.org/avr-libc/user-manual/group__avr__interrupts.html
 * to make sure you don't use depricated names.
 */

/* Interrupt on character received via the USART */
ISR(USART_RX_vect) {
    // Write the received character to the buffer
    *(recv_cmd_state_ptr -> rbuffer_write_ptr) = UDR0;
    if (*(recv_cmd_state_ptr -> rbuffer_write_ptr) == '\r') {
        logger_msg_p("rxchar",log_level_ISR,
            PSTR("Received a command terminator.\r\n"));
        if ((recv_cmd_state_ptr -> rbuffer_count) == 0) {
            /* We got a terminator, but the received character buffer is
             * empty.  The user is trying to clear the transmit and
             * receive queues. */
            return;
        }
        else {
            if ((recv_cmd_state_ptr -> pbuffer_lock) == 1) {
                /* We got a terminator, and there are characters in the received
                 * character buffer, but the parse buffer is locked.  This is
                 * bad -- we're receiving commands faster than we can process
                 * them. */
                logger_msg_p("rxchar",log_level_ERROR,
                    PSTR("Command process speed error!\r\n"));
                rbuffer_erase(recv_cmd_state_ptr);
                return;
            }
            else {
                /* We got a terminator, and there are characters in the received
                 * character buffer.  The parse buffer is unlocked so terminate
                 * the received string and copy it to the parse buffer. */
                *(recv_cmd_state_ptr -> rbuffer_write_ptr) = '\0';
                strcpy((recv_cmd_state_ptr -> pbuffer),
                    (recv_cmd_state_ptr -> rbuffer));
                recv_cmd_state_ptr -> pbuffer_lock = 1;
                logger_msg_p("rxchar",log_level_ISR,
                    PSTR("Parse buffer contains '%s'.\r\n"),
                    (recv_cmd_state_ptr -> pbuffer));
                rbuffer_erase(recv_cmd_state_ptr);
                return;
            }
        }
    }
    else {
        // The character is not a command terminator.
        (recv_cmd_state_ptr -> rbuffer_count)++;
        logger_msg_p("rxchar",log_level_ISR,
            PSTR("%c  <-- copied to receive buffer.  Received count is %d.\r\n"),
            *(recv_cmd_state_ptr -> rbuffer_write_ptr),
            recv_cmd_state_ptr -> rbuffer_count);
        if ((recv_cmd_state_ptr -> rbuffer_count) >= (RECEIVE_BUFFER_SIZE-1)) {
            logger_msg_p("rxchar",log_level_ERROR,
                PSTR("Received character number above limit.\r\n"));
            rbuffer_erase(recv_cmd_state_ptr);
            return;
        }
        else {
            // Increment the write pointer
            (recv_cmd_state_ptr -> rbuffer_write_ptr)++;
        }
    }
    return;
}

