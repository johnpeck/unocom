namespace eval unocom {
    # Procedures and variables unique to the unocom project
    
    proc init {channel} {
	connection::get_unsolicited $channel 100
	sendcmd $channel "loglev 3"
	connection::get_unsolicited $channel 100
    }

    proc sendcmd {channel data} {
	puts -nonewline $channel "$data\r"
	after 100
    }

    proc readline {channel} {
	set data [chan gets $channel]
	return $data
    }
}
